import org.apache.spark.sql._
import org.apache.log4j._
import org.apache.spark.SparkContext
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.to_json


object historicalJson {
  def main(args: Array[String]) {

    // Set the log level to only print errors
    Logger.getLogger("org").setLevel(Level.ERROR)
    val sc = new SparkContext("local[*]", "RatingsCounter")
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    val historicalData = sqlContext.read.json("C:\\Users\\nurit\\IdeaProjects\\untitled1\\src\\main\\scala\\historical")
    val cloumns_names = historicalData.columns.toSeq.drop(1)
    historicalData.show()
    val historicalDataInjson = cloumns_names.foldLeft(historicalData)((historicalData, subvalues) => historicalData.withColumn(subvalues, to_json(struct(col(subvalues)))))
    val subvalue1 = historicalDataInjson.rdd.map( x=> (x(0),x(1))).groupByKey()
    val grouped_by_id = (2 until cloumns_names.length + 1).foldLeft(subvalue1)((subvalue1, i) => subvalue1.union(historicalDataInjson.rdd.map( x=> (x(0),x(i))).groupByKey()))
    val grouped_by_id_and_sub =  grouped_by_id.groupByKey()
    grouped_by_id_and_sub.foreach(println)
  }
}
