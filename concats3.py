# -*- coding: utf-8 -*-
"""
Created on Sun Mar 22 10:29:44 2020

@author: nurit
"""


# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 09:42:54 2020

@author: nurit
"""


import boto3
import re
import os
import uuid

client = boto3.client(
    's3',
    # Hard coded strings as credentials, not recommended.
    aws_access_key_id='public_key',
    aws_secret_access_key='public_secret',
    endpoint_url='http://bdservices-minio.idf-cts.com:9000'
   )

appended_file = 'C:\\Users\\nurit\\appended_objects.txt'
temp_file = 'C:\\Users\\nurit\\temp.txt'
max_apeended_size = 2000
min_appended_size =1000

def upload_object(path, bucket,key):
    with open(path, 'rb') as data:
        client.upload_fileobj(data,bucket,key)

def read_data_from_file(filename):
   fin = open(filename)
   data = fin.read()
   fin.close()
   return data 

def delete_the_uploaded_files(keys,bucket):
    for key in keys:
        client.delete_objects(Bucket=bucket, 
                              Delete={'Objects': [{'Key': key}]}, RequestPayer='requester')

def get_folder_name(key):
    x = re.search(r".*\/",key)
    if x is None:
        return ''
    return x.group()
    
def append_to_file(file_name,data):
    with open(file_name, "a") as myfile:
        myfile.write(data)
        myfile.close()

def append_objects(object_key,bucket):
       client.download_file(bucket,object_key, temp_file)
       data = read_data_from_file(temp_file)
       append_to_file(appended_file, data)
    
def fix_sizes_Withdownload(objects,bucket):
    files =[]
    current_folder = get_folder_name(objects[0]['Key'])
    for _object in objects:
        if _object['Size'] <= min_appended_size:
            if os.path.getsize(appended_file) + _object['Size'] <= max_apeended_size and get_folder_name(_object['Key']) == current_folder:
                append_objects(_object['Key'],bucket)
                files.append(_object['Key'])
            else:
                upload_object(appended_file,bucket,current_folder + str(uuid.uuid1()))
                open(appended_file, 'w').close()
                append_objects(_object['Key'],bucket)
                delete_the_uploaded_files(files,bucket)
                files = []
                files.append(_object['Key'])
                current_folder = get_folder_name(_object['Key'])
    if len(files) > 0:
        upload_object(appended_file,bucket,get_folder_name(files[0]) + str(uuid.uuid1()))
        delete_the_uploaded_files(files,bucket)
    open(appended_file, 'w').close()
    
def concat_files():
    for bucket in client.list_buckets()['Buckets']:
        result = client.list_objects(Bucket=bucket['Name'])
        if 'Contents' in result:
            fix_sizes_Withdownload(result['Contents'],bucket['Name'])
        
concat_files()
        
        
        